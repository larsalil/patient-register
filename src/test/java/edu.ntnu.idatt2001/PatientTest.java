package edu.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    private Patient patient;

    @BeforeEach
    public void init() {
        patient = new Patient("Joe", "Star", "79922971723");
    }

    @Test
    @DisplayName("Changing patient data works")
    public void changingPatientDataTest() {
        patient.setFirstName("Ball");
        assertEquals("Ball", patient.getFirstName());
        patient.setLastName("Karison");
        assertEquals("Karison", patient.getLastName());
        patient.setSocialSecurityNumber("12345678910");
        assertEquals("12345678910", patient.getSocialSecurityNumber());


    }
}
