package edu.ntnu.idatt2001;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class FileHandlerTest {
    private FileHandler fileHandler;
    private PatientRegister patients;
    private final File file = new File("src\\test\\resources\\edu.ntnu.idatt2001." +
            "\\testRegister.csv");
    private final String path = file.getAbsolutePath();

    @BeforeEach
    public void init() {
        fileHandler = new FileHandler();
        patients = new PatientRegister();
        fillWithPatients();
        System.out.println(path);
    }

    /**
     * Fills the register with test data
     */
    private void fillWithPatients() {
        this.patients.addPatient(new Patient("Yo", "Mama", "12345678910"));
        this.patients.addPatient(new Patient("Ole", "Olêolêolê", "21436587091"));
        this.patients.addPatient(new Patient("Shhhhhh", "eeeeet", "10987654324"));
    }

    @Test
    @DisplayName("Writing to file")
    public void writeToFileTest() {
        assertDoesNotThrow(() -> fileHandler.writeCSVFile(patients.getPatients(),path));
    }

    @Test
    @DisplayName("Reading from file")
    public void readFromFileTest() {
        assertDoesNotThrow(() -> fileHandler.readCSVFile(path));
    }

    @Test
    @DisplayName("Reading empty file")
    public void readingEmptyFileTest() throws IOException {
        File emptyFile = new File("src\\test\\resources\\edu.ntnu.idatt2001." +
                "\\emptyTestRegister.csv");
        String emptyFilePath = emptyFile.getAbsolutePath();
        ArrayList<Patient> patients = fileHandler.readCSVFile(emptyFilePath);
        assertTrue(patients.isEmpty());
    }

    @Test
    @DisplayName("The read file is the same as the written file")
    public void writingThenReadingTest() throws IOException {
        fileHandler.writeCSVFile(patients.getPatients(),path);
        ArrayList<Patient> fromFile = fileHandler.readCSVFile(path);
        assertEquals(patients.getPatients(),fromFile);
    }

    @Test
    @DisplayName("Patient data is preserved when saved then read from file")
    public void dataIsPreservedTest() throws IOException {
        Patient patient = new Patient("Yo", "Mama", "12345678910");
        patients.addPatient(patient);
        fileHandler.writeCSVFile(patients.getPatients(),path);
        ArrayList<Patient> fromFile = fileHandler.readCSVFile(path);
        Patient patientFromFile = fromFile.get(0);
        assertEquals(patient.getFirstName(),patientFromFile.getFirstName());
        assertEquals(patient.getLastName(),patientFromFile.getLastName());
        assertEquals(patient.getSocialSecurityNumber(),patientFromFile.getSocialSecurityNumber());
        assertEquals(patient,patientFromFile);
    }
}
