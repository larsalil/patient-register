package edu.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {
    private PatientRegister patients;

    @BeforeEach
    public void init() {
        patients = new PatientRegister();
        fillWithPatients();
    }

    /**
     * Fills the register with test data
     */
    private void fillWithPatients() {
        this.patients.addPatient(new Patient("Yo", "Mama", "12345678910"));
        this.patients.addPatient(new Patient("Ole", "Olêolêolê", "21436587091"));
        this.patients.addPatient(new Patient("Shhhhhh", "eeeeet", "10987654324"));
    }

    @Test
    @DisplayName("Add patient adds a patient")
    public void addPatientTest() {
        Patient patient = new Patient("Joe","Star","79922971723");
        assertTrue(patients.addPatient(patient));
    }

    @Test
    @DisplayName("Remove patient removes the patient")
    public void removePatientTest() {
        Patient patient = new Patient("Yo", "Mama", "12345678910");
        patients.removePatient(patient);
        assertFalse(patients.getPatients().contains(patient));
    }
}
