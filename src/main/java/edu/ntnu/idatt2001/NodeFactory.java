package edu.ntnu.idatt2001;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


/**
 * The type Node factory.
 */
public class NodeFactory {


    /**
     * Create node.
     *
     * @param nodeType the node type
     * @return the node
     */
    public Node create(String nodeType) {

        if (nodeType.equalsIgnoreCase("TOOLBAR")) {
            return new ToolBar();
        }
        else if (nodeType.equalsIgnoreCase("MENUBAR")) {
            return new MenuBar();
        }
        else if (nodeType.equalsIgnoreCase("IMAGEVIEW")) {
            return new ImageView();
        }
        else if (nodeType.equalsIgnoreCase("TABLEVIEW")) {
            return new TableView<>();
        }
        else if (nodeType.equalsIgnoreCase("BUTTON")) {
            return new Button();
        }
        else if (nodeType.equalsIgnoreCase("LABEL")) {
            return new Label();
        }
        else if (nodeType.equalsIgnoreCase("TEXTFIELD")) {
            return new TextField();
        }
        else if (nodeType.equalsIgnoreCase("BORDERPANE")) {
            return new BorderPane();
        }
        else if (nodeType.equalsIgnoreCase("VBOX")) {
            return new VBox();
        }
        else if (nodeType.equalsIgnoreCase("HBOX")) {
            return new HBox();
        }
        return null;
    }
}
