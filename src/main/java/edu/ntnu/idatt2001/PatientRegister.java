package edu.ntnu.idatt2001;

import java.util.ArrayList;

/**
 * The type Patient register.
 */
public class PatientRegister {
    private ArrayList<Patient> patients;

    /**
     * Instantiates a new Patient register.
     */
    public PatientRegister() {
        this.patients = new ArrayList<>();
        //this.fillWithPatients();
    }

    private void fillWithPatients() {
        this.patients.add(new Patient("Yo", "Mama", "12345678910"));
        this.patients.add(new Patient("Ole", "Olêolêolê", "21436587091"));
        this.patients.add(new Patient("Shhhhhh", "eeeeet", "10987654324"));
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public ArrayList<Patient> getPatients() {
        return this.patients;
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public boolean addPatient(Patient patient) {
        this.patients.add(patient);
        return true;
    }


    /**
     * Remove patient.
     *
     * @param patient the patient
     */
    public boolean removePatient(Patient patient) {
        this.patients.remove(patient);
        return true;
    }

    /**
     * Clear register.
     */
    public void clearRegister() {
        patients.clear();
    }




}


