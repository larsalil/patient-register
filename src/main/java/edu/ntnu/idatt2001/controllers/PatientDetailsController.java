package edu.ntnu.idatt2001.controllers;

import edu.ntnu.idatt2001.Patient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * The type Patient details controller.
 */
public class PatientDetailsController {
    @FXML
    public TextField PatientDetailsFirstNameTextField,
            PatientDetailsSocialSecurityNumberTextField,
            PatientDetailsLastNameTextField;
    @FXML
    public Button PatientDetailsOKButton, PatientDetailsCancelButton;
    private Boolean CreatePatientValid = false;

    /**
     * Utility function for filling in the data of the selected patient in the case where
     * editing patient is selected.
     *
     * @param patient the patient
     */
    public void initData(Patient patient) {
        PatientDetailsFirstNameTextField.setText(patient.getFirstName());
        PatientDetailsLastNameTextField.setText(patient.getLastName());
        PatientDetailsSocialSecurityNumberTextField.setText(patient.getSocialSecurityNumber());
    }

    /**
     * Handles if an inputted patient should be allowed to be added to the register.
     *
     * @param actionEvent the action event
     */
    public void PatientDetailsOKButtonPressed(ActionEvent actionEvent) {
        if (validInput()) {
            CreatePatientValid = true;
            Stage stage = (Stage) PatientDetailsCancelButton.getScene().getWindow();
            stage.close();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Input not valid. Every field must be filled out, " +
                    "and Social Security number must be 11 characters long and only contain numbers.");
            alert.showAndWait();
        }
    }

    /**
     * Closes the Patient Details window.
     *
     * @param actionEvent the action event
     */
    public void PatientDetailsCancelButtonPressed(ActionEvent actionEvent) {
        Stage stage = (Stage) PatientDetailsCancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Valid input checker.
     * Checks if any inputs are blank, and that the Social Security Number is valid (dupes are allowed)
     * Would like to move this check to Patient instead.
     *
     * @return the boolean
     */
    public Boolean validInput() {
        if (getFirstName().trim().isBlank() || getLastName().trim().isBlank() || getSocialSecurityNumber().trim().isBlank()) {
            return false;
        }
        return getSocialSecurityNumber().length() == 11 && getSocialSecurityNumber().matches("[0-9]+");
    }


    /**
     * Variable for PrimaryController to get to check if the patient should be added or not.
     * Could probably be replaced with a get for validInput instead.
     *
     * @return the boolean
     */
    public Boolean isCreatePatientValid() {
        return CreatePatientValid;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return PatientDetailsFirstNameTextField.getText().trim();
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return PatientDetailsLastNameTextField.getText().trim();
    }

    /**
     * Gets social security number.
     *
     * @return the social security number
     */
    public String getSocialSecurityNumber() {
        return PatientDetailsSocialSecurityNumberTextField.getText().trim();
    }

    /**
     * Creates a patient and returns it.
     * For use in PrimaryController.
     *
     * @return the patient
     */
    public Patient getPatient() {
        return new Patient(getFirstName(), getFirstName(), getSocialSecurityNumber());
    }
}
