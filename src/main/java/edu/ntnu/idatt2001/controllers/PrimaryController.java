package edu.ntnu.idatt2001.controllers;


import edu.ntnu.idatt2001.App;
import edu.ntnu.idatt2001.FileHandler;
import edu.ntnu.idatt2001.Patient;
import edu.ntnu.idatt2001.PatientRegister;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Primary controller.
 */
public class PrimaryController implements Initializable {
    private PatientRegister patients;
    private ObservableList<Patient> observablePatientList;


    @FXML
    public MenuItem MenuFileImportFromCSVButton,
            MenuFileExportToCSVButton,
            MenuFileExitButton,
            MenuEditAddPatientButton,
            MenuEditEditPatientButton,
            MenuEditRemovePatientButton,
            MenuHelpAboutButton;

    @FXML
    public Button
            AddPatientButton,
            EditPatientButton,
            RemovePatientButton;

    @FXML
    public TableView<Patient> tableView;

    @FXML
    public TableColumn<Patient, String> firstNameColumn,
    lastNameColumn,
    socialSecurityNumberColumn;

    /**
     * Imports a patient register from a chosen .csv file.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void MenuFileImportFromCSVButtonPressed(ActionEvent actionEvent) {
        FileHandler fileHandler = new FileHandler();
        FileChooser fileChooser = fileChooserSetUp();
        File selectedFile = fileChooser.showOpenDialog(new Stage());

        if (selectedFile != null) {
            ArrayList<Patient> readPatients;
            try {
                patients.clearRegister();
                readPatients = fileHandler.readCSVFile(selectedFile.getAbsolutePath());
                for (Patient p : readPatients) {
                    try {
                        patients.addPatient(p);
                    } catch (IllegalArgumentException e) {
                        System.out.println("Patient \"" + p + "\" not added: " + e.getMessage());
                    }
                }
                updateObservableList();
                System.out.println("Import successful!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Import cancelled.");
        }
    }

    /**
     * Exports current patient register to a chosen .csv file.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void MenuFileExportToCSVButtonPressed(ActionEvent actionEvent){
        FileHandler fileHandler = new FileHandler();
        FileChooser fileChooser = fileChooserSetUp();
        File selectedFile = fileChooser.showSaveDialog(new Stage());

        if(selectedFile != null) {
            try {
                fileHandler.writeCSVFile(patients.getPatients(), selectedFile.getAbsolutePath());
                System.out.println("Export successful!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Export to file cancelled");
        }
    }

    /**
     * Exits the application.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void MenuFileExitButtonPressed(ActionEvent actionEvent) {
        Platform.exit();
    }

    /**
     * Adds a patient to the patient register
     */
    @FXML
    public void AddPatientButtonPressed(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(App.class.getResource("patient-details.fxml"));
            Parent root1 = loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Patient Details");
            stage.setScene(new Scene(root1));
            PatientDetailsController controller = loader.getController();
            stage.showAndWait();
            if (controller.isCreatePatientValid()) {
                patients.addPatient(controller.getPatient());
                updateObservableList();
            }
        } catch (Exception e) {
            System.out.println("Can't load new window");
        }
    }

    /**
     * Edits a patient in the patient register
     *
     * @param actionEvent the action event
     */
    @FXML
    public void EditPatientButtonPressed(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(App.class.getResource("patient-details.fxml"));
            Parent root1 = loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Patient Details");
            stage.setScene(new Scene(root1));
            PatientDetailsController controller = loader.getController();
            Patient selectedPatient = tableView.getSelectionModel().getSelectedItem();
            controller.initData(selectedPatient);
            stage.showAndWait();
            if (controller.isCreatePatientValid()) {
                patients.removePatient(selectedPatient);
                patients.addPatient(controller.getPatient());
                updateObservableList();
            }
        } catch (Exception e) {
            System.out.println("Can't load new window");
        }
    }

    /**
     * Removes a patient from the patient register
     *
     * @param actionEvent the action event
     */
    @FXML
    public void removePatientButtonPressed(ActionEvent actionEvent) {
        try {
            Patient selectedPatient = tableView.getSelectionModel().getSelectedItem();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm deletion");
            alert.setHeaderText("Confirm deletion of selected patient");
            alert.setContentText("Are you sure you wish to delete this patient? \n" + selectedPatient.toString());
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                patients.removePatient(selectedPatient);
                updateObservableList();
            }
        } catch (Exception e) {
            System.out.println("Can't load new window");
        }
    }

    /**
     * Brings up an About window.
     *
     * @param actionEvent the action event
     */
    @FXML
    public void MenuHelpAboutButtonPressed(ActionEvent actionEvent) {
        try {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("About");
            alert.setHeaderText("Patient Register\n" + "v0.1");
            alert.setContentText("Created for GetDownWithTheSickness Hospital\n" + "Created by Lars Andreas Lillehaug");
            alert.showAndWait();
        } catch (Exception e) {
            System.out.println("Can't load new window");
        }
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.patients = new PatientRegister();

        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        this.observablePatientList = FXCollections.observableArrayList((this.patients.getPatients()));
        this.tableView.setItems(this.observablePatientList);
    }

    /**
     * Updates the patient register display
     */
    private void updateObservableList() {
        this.observablePatientList.setAll(this.patients.getPatients());
    }

    /**
     * Set up for the FileChooser
     * default directory: CSV folder inside the resources folder
     * file filter: only .csv files are allowed
     *
     */
    private FileChooser fileChooserSetUp() {
        String path = FileSystems.getDefault().getPath("").toAbsolutePath() +
                "\\src\\main\\resources\\edu\\ntnu\\idatt2001\\CSV";
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        return fc;
    }
}
