package edu.ntnu.idatt2001;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The type File handler.
 */
public class FileHandler {

    /**
     * Instantiates a new File handler.
     */
    public FileHandler() {
    }

    /**
     * Read a .csv file into the patient register. ; is used as the separator.
     * The .cvs file given for this task contains firstName;lastName;generalPractitioner;socialSecurityNumber
     * Since the application does not support adding general practitioners, the third column is currently skipped.
     *
     * @param filePath the file path
     * @return the array list
     * @throws IOException the io exception
     */
    public ArrayList<Patient> readCSVFile(String filePath) throws IOException {
        ArrayList<Patient> readPatientRegister = new ArrayList<>();
        try (FileReader fr = new FileReader(filePath)) {
            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine(); // skips identifier line
            line = br.readLine();

            while (line != null) {
                String[] list = line.split(";");
                try {
                    Patient newPatient = new Patient(list[0],list[1],list[3]); //firstName, lastName, SSnumber
                    readPatientRegister.add(newPatient);
                } catch (IllegalArgumentException e) {
                    System.out.println("Patient could not be created: " + e.getMessage());
                }
                line = br.readLine();
            }
        } catch (IOException e) {
            throw new IOException("File could not be opened at: " + filePath);
        }
        return readPatientRegister;
    }

    /**
     * Write a .csv from the patient register. ; is used as the separator.
     * Since the application does not support generalPractitioner, the third column will be saved as null.
     *
     * @param patientList the patient list
     * @param filePath    the file path
     * @throws IOException the io exception
     */
    public void writeCSVFile(ArrayList<Patient> patientList, String filePath) throws IOException {
        try (FileWriter fw = new FileWriter(String.valueOf(filePath))) {
            fw.write("firstName;lastName;generalPractitioner;socialSecurityNumber;\n");

            for (Patient p : patientList) {
                String line = p.getFirstName() + ";" + p.getLastName() + ";" + p.getGeneralPractitioner() + ";"
                        + p.getSocialSecurityNumber() + ";" + "\n";
                fw.append(line);
            }
            System.out.println("Patients saved!");
        } catch (IOException e) {
            throw new IOException("File could not saved at: " + filePath + "\n" + e.getMessage());
        }
    }


}
