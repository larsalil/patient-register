module edu.ntnu.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001 to javafx.fxml;
    exports edu.ntnu.idatt2001;
    exports edu.ntnu.idatt2001.controllers;
    opens edu.ntnu.idatt2001.controllers to javafx.fxml;
}